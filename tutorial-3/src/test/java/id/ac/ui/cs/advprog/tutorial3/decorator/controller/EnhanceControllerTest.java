package id.ac.ui.cs.advprog.tutorial3.decorator.controller;


import id.ac.ui.cs.advprog.tutorial3.decorator.service.EnhanceService;
import id.ac.ui.cs.advprog.tutorial3.decorator.service.EnhanceServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = EnhanceController.class)
public class EnhanceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EnhanceServiceImpl enhanceService;

    @Test
    public void whenHomeUrlIsAccessedItShouldContainCorrectWeaponsModel() throws Exception {

        mockMvc.perform(get("/home"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("weapons"))
                .andExpect(view().name("decorator/home"));
    }

    @Test
    public void whenEnhanceUrlIsAccessedItShouldEnhanceAllWeapons() throws Exception {

        mockMvc.perform(post("/enhance"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/home"));
    }

}
