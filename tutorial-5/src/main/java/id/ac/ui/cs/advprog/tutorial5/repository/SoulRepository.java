package id.ac.ui.cs.advprog.tutorial5.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;

public interface SoulRepository extends JpaRepository<Soul, Long> {
}
