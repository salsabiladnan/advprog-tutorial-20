package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

public enum EnhancerDecorator {
    CHAOS_UPGRADE,
    MAGIC_UPGRADE,
    RAW_UPGRADE,
    REGULAR_UPGRADE,
    UNIQUE_UPGRADE;

    public Weapon addWeaponEnhancement(Weapon weapon) {

        if (this == EnhancerDecorator.RAW_UPGRADE) {
            weapon = new RawUpgrade(weapon);
        } else if (this == EnhancerDecorator.CHAOS_UPGRADE) {
            weapon = new ChaosUpgrade(weapon);
        } else if (this == EnhancerDecorator.MAGIC_UPGRADE) {
            weapon = new MagicUpgrade(weapon);
        } else if (this == EnhancerDecorator.REGULAR_UPGRADE) {
            weapon = new RegularUpgrade(weapon);
        } else if (this == EnhancerDecorator.UNIQUE_UPGRADE) {
            weapon = new UniqueUpgrade(weapon);
        } else {
            weapon = new RegularUpgrade(weapon);
        }

        return weapon;

    }
}
